Workflow Engine VS. State Machine
=================================
The `major difference` between a workflow engine and a state machine lies in focus. In a workflow engine, 
`transition to the next step` occurs when a previous action is completed, 
whilst a state machine needs an external event that will cause branching to the next activity. 
In other words, state machine is event driven and workflow engine is not.

## What to Consider When Making a Choice

**Flexibility vs Changes**
* State machine works asynchronously and the steps in the machine are triggered by certain events/actions,
they shouldn't necessarily be performed in a strict order from this point of view state machines are more flexible.
* Workflow engine is a better option. when business rules change frequently.

**Understandability vs Readability**
* State machine suites for one-dimensional problems and they fail to handle more complex systems.
* Workflow is the most appropriate solution for larger systems

>State machine is a good solution if your system is not very complex, and if we are capable of drawing all the possible states as well as the events that will cause transitions to them in the first place.



## Features that are supported by `SSM`
* _State persistence_	
* _Distributed states_
* _Versioning_
* _Installing the process in an arbitrary state_
* _Timers and delays_
* _Modifying schemes at runtime_
* _Visual Designer_

##State Machine Major Flaws
>if the system that is being implemented is complex then code will become `unreadable`, may encounter a problem of `unreachable states` and  difficulty to `maintainable` the State Machine.

##State machine is not the best choice when
* You cannot break code into states
* The number of states is indefinite
* You want to execute several states in parallel
* Your algorithm is too simple or too complex



